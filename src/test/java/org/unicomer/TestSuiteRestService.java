package org.unicomer;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        features = "src/test/resources/features/servicesRest.feature",
        tags = "@TestRestPostTokenUnicomer",
        glue = "org.unicomer.stepDefinitions"
)

public class TestSuiteRestService {

}
