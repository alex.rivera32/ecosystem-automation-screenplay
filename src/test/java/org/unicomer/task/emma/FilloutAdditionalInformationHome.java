package org.unicomer.task.emma;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.userInterfaces.emma.EmmaAdditionalnformationHome;
import org.unicomer.utils.SelectList;
import org.unicomer.utils.Wait;

public class FilloutAdditionalInformationHome implements Task  {
    private final DataTable customer;
    public FilloutAdditionalInformationHome(DataTable customer) {
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(customer.cell(1,0)).into(EmmaAdditionalnformationHome.INPUT_STREET_HOME));
        actor.attemptsTo(Enter.theValue(customer.cell(1,1)).into(EmmaAdditionalnformationHome.INPUT_REFERENCE_STREET));
        actor.attemptsTo(Click.on(EmmaAdditionalnformationHome.LIST_DATE_HOME));
        actor.attemptsTo(SelectList.since(EmmaAdditionalnformationHome.LIST_SELECT_DATE_HOME,"2020",SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaAdditionalnformationHome.LIST_SELECT_DATE_HOME,customer.cell(1,3),SelectList.getDate()));
        actor.attemptsTo(Click.on(EmmaAdditionalnformationHome.LIST_TYPE_HOME));
        actor.attemptsTo(SelectList.since(EmmaAdditionalnformationHome.LIST_ELEMENTS_TYPE_HOME,customer.cell(1,4),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaAdditionalnformationHome.BTN_NEXT_REFERENCES));
        Wait.to(3);
     }
    public static FilloutAdditionalInformationHome customer(DataTable customer) {
        return Tasks.instrumented(FilloutAdditionalInformationHome.class,customer);
    }
}
