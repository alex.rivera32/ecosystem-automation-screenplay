package org.unicomer.task.emma;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.userInterfaces.emma.EmmaIAdditionalInformation;
import org.unicomer.utils.*;

public class FilloutAdditionalInformation implements Task  {
    private final DataTable customer;
    public FilloutAdditionalInformation(DataTable customer) {
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(EmmaIAdditionalInformation.LIST_COUNTRY_BIRTH).afterWaitingUntilPresent());
        actor.attemptsTo(SelectList.since(EmmaIAdditionalInformation.LIST_ELEMENTS_COUNTRY_BIRTH,customer.cell(1,0),SelectList.getCountry()));
        actor.attemptsTo(Enter.theValue(customer.cell(1,1)).into(EmmaIAdditionalInformation.INPUT_DEPENDENTS));
        actor.attemptsTo(Click.on(EmmaIAdditionalInformation.LIST_TYPE_PHONE));
        actor.attemptsTo(SelectList.since(EmmaIAdditionalInformation.LIST_ELEMENTS_TYPE_PHONE,customer.cell(1,2),SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.cell(1,3)).into(EmmaIAdditionalInformation.INPUT_PHONE_NUMBER));
        actor.attemptsTo(Click.on(EmmaIAdditionalInformation.BTN_NEXT));
        Wait.to(3);
    }
    public static FilloutAdditionalInformation customer(DataTable customer) {
        return Tasks.instrumented(FilloutAdditionalInformation.class,customer);
    }
}
