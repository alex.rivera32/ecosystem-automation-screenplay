package org.unicomer.task.emma;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;

public class DocumentValidation implements Task {

    private static String documentId;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(SendKeys.of(documentId).into(EmmaInterfazBasicInformation.INPUT_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_SEARCH_CUSTOMER));
    }

    public static DocumentValidation insert(String id) {
        documentId = id;
        return Tasks.instrumented(DocumentValidation.class);
    }
}
