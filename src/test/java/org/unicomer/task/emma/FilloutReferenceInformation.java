package org.unicomer.task.emma;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.userInterfaces.emma.EmmaReferenceInformation;
import org.unicomer.utils.SelectList;
import org.unicomer.utils.Wait;

public class FilloutReferenceInformation implements Task  {
    private final DataTable customer;
    public FilloutReferenceInformation(DataTable customer) {
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(customer.cell(1,0)).into(EmmaReferenceInformation.INPUT_NAME_FIRST_REFERENCE));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_RELATIONSHIP_FIRST_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.cell(1,1),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_TYPE_PHONE_FIST_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.cell(1,2),SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.cell(1,3)).into(EmmaReferenceInformation.INPUT_PHONE_FIRST_REFERENCE));
        actor.attemptsTo(Enter.theValue(customer.cell(1,4)).into(EmmaReferenceInformation.INPUT_NAME_SECOND_REFERENCE));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_RELATIONSHIP_SECOND_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.cell(1,5),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_TYPE_PHONE_SECOND_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.cell(1,6),SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.cell(1,7)).into(EmmaReferenceInformation.INPUT_PHONE_SECOND_REFERENCE));
        actor.attemptsTo(Enter.theValue(customer.cell(1,8)).into(EmmaReferenceInformation.INPUT_NAME_THIRD_REFERENCE));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_RELATIONSHIP_THIRD_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.cell(1,9),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.LIST_TYPE_PHONE_THIRD_REFERENCE));
        actor.attemptsTo(SelectList.since(EmmaReferenceInformation.LIST_ELEMENTS,customer.cell(1,10),SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.cell(1,11)).into(EmmaReferenceInformation.INPUT_PHONE_THIRD_REFERENCE));
        actor.attemptsTo(Click.on(EmmaReferenceInformation.BTN_NEXT_DOCUMENTS));
        Wait.to(3);
    }
    public static FilloutReferenceInformation customer(DataTable customer) {
        return Tasks.instrumented(FilloutReferenceInformation.class,customer);
    }
}
