package org.unicomer.task.emma;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.userInterfaces.emma.EmmaWorkInformation;
import org.unicomer.utils.*;

public class FilloutWorkInformation implements Task  {
    private final DataTable customer;
    public FilloutWorkInformation(DataTable customer) {
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(customer.cell(1,0)).into(EmmaWorkInformation.INPUT_NAME_COMPANY));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_DATE_COMPANY));
        actor.attemptsTo(Click.on(EmmaWorkInformation.BTN_SELECT_YEARS_COMPANY));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_SELECT_DATE_COMPANY,customer.cell(1,1),SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_SELECT_DATE_COMPANY,customer.cell(1,2),SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_SELECT_DATE_COMPANY,customer.cell(1,3),SelectList.getDate()));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_OCUPATION));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS_OCUPATION,customer.cell(1,4),SelectList.getOcupation()));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_DEPARTMENT));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS,customer.cell(1,5),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_DISTRICT));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS,customer.cell(1,6),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_NEIGHBORHOOD));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS,customer.cell(1,7),SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.cell(1,8)).into(EmmaWorkInformation.INPUT_STREET_WORK));
        actor.attemptsTo(Enter.theValue(customer.cell(1,9)).into(EmmaWorkInformation.INPUT_REFERENCE_STREET_WORK));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_TYPE_PHONE1));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS,customer.cell(1,10),SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.cell(1,11)).into(EmmaWorkInformation.INPUT_PHONE_NUMBER1));
        actor.attemptsTo(Click.on(EmmaWorkInformation.LIST_TYPE_PHONE2));
        actor.attemptsTo(SelectList.since(EmmaWorkInformation.LIST_ELEMENTS,customer.cell(1,12),SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.cell(1,13)).into(EmmaWorkInformation.INPUT_PHONE_NUMBER2));
        actor.attemptsTo(Click.on(EmmaWorkInformation.BTN_NEXT_ADDITIONAL_HOME));
        Wait.to(3);
    }
    public static FilloutWorkInformation customer(DataTable customer) {
        return Tasks.instrumented(FilloutWorkInformation.class,customer);
    }
}
