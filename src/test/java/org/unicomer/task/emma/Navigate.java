package org.unicomer.task.emma;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Open;
import org.unicomer.constants.SystemConstants;
import org.unicomer.userInterfaces.emma.EmmaCredentials;
import org.unicomer.utils.ConstantReader;
import org.unicomer.utils.Wait;

public class Navigate implements Task {
    public Navigate(String pageEmma) {
        this.pageEmma=pageEmma;
    }
    private static String pageEmma;
    private static String username;
    private static String password;


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.url(pageEmma));
        actor.attemptsTo(Click.on(EmmaCredentials.BTN_START));
        actor.attemptsTo(Enter.theValue(username).into(EmmaCredentials.INPUT_USERNAME));
        actor.attemptsTo(Click.on(EmmaCredentials.BTN_SEND_CREDENTIAL));
        actor.attemptsTo(Enter.theValue(password).into(EmmaCredentials.INPUT_PASSWORD));
        actor.attemptsTo(Click.on(EmmaCredentials.BTN_SEND_CREDENTIAL));
        actor.attemptsTo(Click.on(EmmaCredentials.BTN_SESSION_ACTIVE));
        Wait.to(3);
    }

    public static Navigate page(String pageEmma) {
        ConstantReader.getInstance().setEnv(pageEmma);
        pageEmma = ConstantReader.getInstance().getProperties(SystemConstants.EMMA_URL);
        username = ConstantReader.getInstance().getProperties(SystemConstants.EMMA_USERNAME);
        password = ConstantReader.getInstance().getProperties(SystemConstants.EMMA_PASSWORD);
        return Tasks.instrumented(Navigate.class,pageEmma);
    }
}
