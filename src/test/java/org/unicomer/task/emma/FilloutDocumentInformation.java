package org.unicomer.task.emma;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import org.unicomer.models.Customer;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.utils.SelectList;
import org.unicomer.utils.Wait;

public class FilloutDocumentInformation implements Task  {
    private final Customer customer;

    public FilloutDocumentInformation(Customer customer) {
        this.customer = customer;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_SELECT));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,customer.getTypeId(),SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.getDocumentId()).into(EmmaInterfazBasicInformation.INPUT_DOCUMENT_CLIENT));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_SEARCH_CUSTOMER));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_START_APPLICATION).afterWaitingUntilPresent());
    }
    public static FilloutDocumentInformation with(Customer customer) {
        return Tasks.instrumented(FilloutDocumentInformation.class,customer);
    }
}
