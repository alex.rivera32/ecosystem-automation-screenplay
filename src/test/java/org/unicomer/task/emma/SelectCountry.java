package org.unicomer.task.emma;

import net.serenitybdd.core.pages.ClickStrategy;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import org.unicomer.constants.SystemConstants;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.utils.ConstantReader;
import org.unicomer.utils.SelectList;
import org.unicomer.utils.Wait;

public class SelectCountry implements Task {

    public static String country;

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_SELECT).withStrategy(ClickStrategy.WAIT_UNTIL_PRESENT));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,country,SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_CONTINUE));
        Wait.to(3);
    }

    public static SelectCountry the() {
        country = ConstantReader.getInstance().getProperties(SystemConstants.COUNTRY);
        return Tasks.instrumented(SelectCountry.class);
    }
}
