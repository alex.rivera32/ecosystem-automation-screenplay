package org.unicomer.task.emma;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import org.unicomer.models.Customer;

public class TestExcel implements Task {

    @Override
    public <T extends Actor> void performAs(T actor) {
        System.out.println(Customer.typeId);
        System.out.println(Customer.documentId);
        System.out.println(Customer.gender);

    }

    public static TestExcel init() {
        return Tasks.instrumented(TestExcel.class);
    }
}
