package org.unicomer.task.emma;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.SendKeys;
import org.unicomer.constants.SystemConstants;
import org.unicomer.userInterfaces.emma.EmmaUploadDocuments;
import org.unicomer.utils.SelectList;
import org.unicomer.utils.UtilsManager;
import org.unicomer.utils.Wait;

public class FilloutUploadDocuments implements Task  {
    private final String lineCredit;
    public FilloutUploadDocuments(String lineCredit) {
        this.lineCredit = lineCredit;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(EmmaUploadDocuments.LIST_LINE_CREDIT_DOCUMENTS).afterWaitingUntilPresent());
        actor.attemptsTo(SelectList.since(EmmaUploadDocuments.LIST_ELEMENTS_DOCUMENTS,lineCredit,SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaUploadDocuments.BTN_DOCUMENT_1));
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaUploadDocuments.BTN_DOCUMENT_2));
        /*actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaUploadDocuments.BTN_DOCUMENT_3));
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaUploadDocuments.BTN_DOCUMENT_4));
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaUploadDocuments.BTN_DOCUMENT_5));
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaUploadDocuments.BTN_DOCUMENT_6));
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaUploadDocuments.BTN_DOCUMENT_7));
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaUploadDocuments.BTN_DOCUMENT_8));*/
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaUploadDocuments.INPUT_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaUploadDocuments.BTN_SENT_APLICATION_FINISH).afterWaitingUntilEnabled());
        Wait.to(5);
    }
    public static FilloutUploadDocuments customer(String lineCredit) {
        return Tasks.instrumented(FilloutUploadDocuments.class,lineCredit);
    }
}
