package org.unicomer.task.emma;

import io.cucumber.datatable.DataTable;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SendKeys;
import org.unicomer.constants.SystemConstants;
import org.unicomer.models.Customer;
import org.unicomer.userInterfaces.emma.EmmaInterfazBasicInformation;
import org.unicomer.utils.*;

public class FilloutBasicInformation implements Task  {
    private final Customer customer;
    private static String email;
    public FilloutBasicInformation(Customer customer) {
        this.customer = customer;
    }
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_LINE_CREDIT).afterWaitingUntilPresent());
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,customer.getCreditLine(),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_DOCUMENT_CREATION_DATE));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_SELECT_YEARS));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_SELECT_DATE,customer.getDcYear(),SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_SELECT_DATE,customer.getDcMonth(),SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_SELECT_DATE,customer.getDcDay(),SelectList.getDate()));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_DOCUMENT_FINAL_DATE));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_SELECT_YEARS));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_SELECT_DATE,customer.getDfYear(),SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_SELECT_DATE,customer.getDfMonth(),SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_SELECT_DATE,customer.getDfDay(),SelectList.getDate()));
        actor.attemptsTo(Enter.theValue(customer.getFirstName()).into(EmmaInterfazBasicInformation.INPUT_NAME));
        actor.attemptsTo(Enter.theValue(customer.getFirstLastName()).into(EmmaInterfazBasicInformation.INPUT_LAST_NAME));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_GENDER));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,customer.getGender(),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_CIVIL_STATUS));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,customer.getCivilStatus(),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_BIRTH_DATE));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_SELECT_YEARS));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_SELECT_DATE,customer.getYear(),SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_SELECT_DATE,customer.getMonth(),SelectList.getDate()));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_SELECT_DATE,customer.getDay(),SelectList.getDate()));
        actor.attemptsTo(Enter.theValue(customer.getNumberMovil()).into(EmmaInterfazBasicInformation.INPUT_PHONE_NUMBER));
        actor.attemptsTo(Enter.theValue(email).into(EmmaInterfazBasicInformation.INPUT_EMAIL));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_SOURCE_INCOME));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,customer.getSourceIncome(),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_FREQUENCY_PAID));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,customer.getFrecuencyPaid(),SelectList.getElement()));
        actor.attemptsTo(Enter.theValue(customer.getIncome()).into(EmmaInterfazBasicInformation.INPUT_INCOME));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_DEPARTMENT));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,customer.getDepartment(),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_DISTRICT));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,customer.getDistrict(),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.LIST_NEIGHBORHOOD));
        actor.attemptsTo(SelectList.since(EmmaInterfazBasicInformation.LIST_ELEMENTS,customer.getNeighborhood(),SelectList.getElement()));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_UPLOAD_DOCUMENT));
        actor.attemptsTo(SendKeys.of(UtilsManager.getInstance().getPathDocument()).into(EmmaInterfazBasicInformation.INPUT_DOCUMENT));
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_SEND_FOR_EVALUATION).afterWaitingUntilEnabled());
        Wait.to(3);
        actor.attemptsTo(Click.on(EmmaInterfazBasicInformation.BTN_PREAPPROVED_OK).afterWaitingUntilPresent());
        Wait.to(3);

    }
    public static FilloutBasicInformation customer(Customer customer) {
        email = UtilsManager.getInstance().getEmail();
        return Tasks.instrumented(FilloutBasicInformation.class,customer);
    }
}
