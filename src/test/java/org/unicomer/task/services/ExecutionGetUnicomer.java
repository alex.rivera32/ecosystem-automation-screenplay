package org.unicomer.task.services;

import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.rest.interactions.Get;
import org.apache.http.HttpHeaders;
import org.unicomer.constants.ConstantRest;
public class ExecutionGetUnicomer implements Task {
    private String endpoint;
    private String id;
    public ExecutionGetUnicomer(String endpoint, String id) {
        this.endpoint = endpoint;
        this.id = id;
    }

    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Get.resource(endpoint.concat(id)).with(request -> request.header(HttpHeaders.CONTENT_TYPE, ContentType.JSON).auth().oauth2(ConstantRest.getTOKEN())
                .header("X-country","PY")
                .and().header("X-channelRef","POS")
                        .and().header("X-channelRef","POS")
                        .and().header("X-brand","UNICOMER")
                        .and().header("X-storeRef","CURACAO")
                        .and().header("X-environment","E2E")
                        .and().header("X-consumerDateTime","026-09-09T09:20:50-05:00")
                        .and().header("X-processRef","Shopping car")
                        .and().header("X-typeProduct","2")));
    }
    public static ExecutionGetUnicomer getInformation(String endpoint, String id) {
        return Tasks.instrumented(ExecutionGetUnicomer.class,endpoint,id);
    }
}
