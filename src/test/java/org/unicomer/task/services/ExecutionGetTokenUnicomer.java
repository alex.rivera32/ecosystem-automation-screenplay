package org.unicomer.task.services;

import io.restassured.http.ContentType;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;

import net.serenitybdd.screenplay.rest.interactions.Post;
import org.apache.http.HttpHeaders;
import org.unicomer.constants.ConstantRest;

public class ExecutionGetTokenUnicomer implements Task {
    private String endpoint;
    private String urlParameters  = "grant_type=client_credentials&client_id=WqAAEv9XmaITHperHFIFHmrnSSCp4VXB2oppGes1LFIEdW7p&client_secret=7k0GscbMotmBZ9MThwbqrjsATAqg9sXGQzMOnVMvj1TjgTEqLECTSA6V459lvywZ";
    public ExecutionGetTokenUnicomer(String endpoint) {
        this.endpoint = endpoint;
    }

    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Post.to(endpoint).with(request ->request.header(HttpHeaders.CONTENT_TYPE, ContentType.URLENC).body(urlParameters)));
        ConstantRest.setTOKEN(SerenityRest.lastResponse().body().jsonPath().getString("access_token"));
    }
    public static ExecutionGetTokenUnicomer getNewToken(String endpoint) {
        return Tasks.instrumented(ExecutionGetTokenUnicomer.class,endpoint);
    }
}
