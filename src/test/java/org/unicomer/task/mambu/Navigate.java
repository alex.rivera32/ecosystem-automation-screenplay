package org.unicomer.task.mambu;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actions.SendKeys;
import org.openqa.selenium.Keys;
import org.unicomer.constants.SystemConstants;
import org.unicomer.userInterfaces.mambu.MambuInterfaz;
import org.unicomer.utils.ConstantReader;

public class Navigate implements Task {

    private static String url;
    private static String username;
    private static String password;


    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.url(url));
        actor.attemptsTo(SendKeys.of(username).into(MambuInterfaz.INPUT_USERNAME));
        actor.attemptsTo(SendKeys.of(password).into(MambuInterfaz.INPUT_PASSWORD).thenHit(Keys.ENTER));
    }

    public static Navigate page(String mambuPage) {
        ConstantReader.getInstance().setEnv(mambuPage);
        url = ConstantReader.getInstance().getProperties(SystemConstants.MAMBU_URL);
        username = ConstantReader.getInstance().getProperties(SystemConstants.MAMBU_USERNAME);
        password = ConstantReader.getInstance().getProperties(SystemConstants.MAMBU_PASSWORD);
        return Tasks.instrumented(Navigate.class);
    }
}
