package org.unicomer.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;
import org.unicomer.userInterfaces.emma.EmmaUploadDocuments;

import java.sql.SQLOutput;

public class TheMessage  implements Question<String> {
    @Override
    public String answeredBy(Actor actor) {
        return Text.of(EmmaUploadDocuments.ALERT_CLIENT_CREATE).asString().answeredBy(actor);
    }

    public static TheMessage is() {
        return new TheMessage();
    }
}