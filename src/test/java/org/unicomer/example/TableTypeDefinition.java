package org.unicomer.example;

import io.cucumber.java.DataTableType;
import org.unicomer.models.Customer;

import java.util.Map;

public class TableTypeDefinition {

    @DataTableType(replaceWithEmptyString = "novalor")
    public Customer createCustomer(Map<String, String> entry){
            return new Customer(entry);
    }
}
