package org.unicomer.models;

import java.util.Map;

public class Customer {
    public static String typeId;
    public static String documentId;
    private String creditLine;
    private String firstName;
    private String secondName;
    private String firstLastName;
    private String secondLastName;
    public static String gender;
    private String civilStatus;
    private String birthDate;
    private String numberMovil;
    private String email;
    private String sourceIncome;
    private String frecuencyPaid;
    private String income;
    private String department;
    private String district;
    private String neighborhood;

    private String year;
    private String month;
    private String day;

    private String fullName;

    private String docCreationDate;
    private String docFinalDate;
    private String dcYear;
    private String dcMonth;
    private String dcDay;
    private String dfYear;
    private String dfMonth;
    private String dfDay;

    public Customer(String property,String value){
        if (property.equals("typeId")) {
            this.typeId = value;
        }else if (property.equals("documentId")) {
            this.documentId = value;
        } else if (property.equals("firstName")) {
            this.firstName = value;
        }else if (property.equals("gender")) {
            this.gender = value;
        }
    }

    public Customer(Map<String, String> entry) {
        this.typeId = entry.get("typeId");
        this.documentId = entry.get("documentId");
        this.creditLine = entry.get("creditLine");
        this.firstName = entry.get("firstName");
        this.secondName = entry.get("secondName");
        this.firstLastName = entry.get("firstLastName");
        this.secondLastName = entry.get("secondLastName");
        this.gender = entry.get("gender");
        this.civilStatus = entry.get("civilStatus");
        this.birthDate = entry.get("birthDate");
        this.numberMovil = entry.get("numberMovil");
        this.email = entry.get("email");
        this.sourceIncome = entry.get("sourceIncome");
        this.frecuencyPaid = entry.get("frecuencyPaid");
        this.income = entry.get("income");
        this.department = entry.get("department");
        this.district = entry.get("district");
        this.neighborhood = entry.get("neighborhood");
        this.docCreationDate = entry.get("docCreationDate");
        this.docFinalDate = entry.get("docFinalDate");
        setFullname();
        setYearMonthDay();
        setDocumentCreationD();
        setDocumentFinalD();
    }

    @Override
    public String toString() {
        return "Customer{" + "typeId='" + typeId  + ", documentId='" + documentId + ", fullName='" + fullName + '}';
    }

    private void setFullname(){
        fullName = firstName + " " + secondName + " " + firstLastName + " " + secondLastName;
    }

    private void setYearMonthDay(){
        if(birthDate == null) return;
        this.year = birthDate.split("-")[0];
        this.month = birthDate.split("-")[1]+ ".";
        this.day = birthDate.split("-")[2];
    }

    private void setDocumentCreationD(){
        if(docCreationDate == null) return;
        this.dcYear = docCreationDate.split("-")[0];
        this.dcMonth = docCreationDate.split("-")[1]+ ".";
        this.dcDay = docCreationDate.split("-")[2];
    }

    private void setDocumentFinalD(){
        if(docFinalDate == null) return;
        this.dfYear = docFinalDate.split("-")[0];
        this.dfMonth = docFinalDate.split("-")[1]+ ".";
        this.dfDay = docFinalDate.split("-")[2];
    }

    public String getFullName() {
        return fullName;
    }

    public String getYear() {
        return year;
    }

    public String getMonth() {
        return month;
    }

    public String getDay() {
        return day;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getCreditLine() {
        return creditLine;
    }

    public void setCreditLine(String creditLine) {
        this.creditLine = creditLine;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(String civilStatus) {
        this.civilStatus = civilStatus;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getNumberMovil() {
        return numberMovil;
    }

    public void setNumberMovil(String numberMovil) {
        this.numberMovil = numberMovil;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSourceIncome() {
        return sourceIncome;
    }

    public void setSourceIncome(String sourceIncome) {
        this.sourceIncome = sourceIncome;
    }

    public String getFrecuencyPaid() {
        return frecuencyPaid;
    }

    public void setFrecuencyPaid(String frecuencyPaid) {
        this.frecuencyPaid = frecuencyPaid;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getFirstLastName() {
        return firstLastName;
    }

    public void setFirstLastName(String firstLastName) {
        this.firstLastName = firstLastName;
    }

    public String getSecondLastName() {
        return secondLastName;
    }

    public void setSecondLastName(String secondLastName) {
        this.secondLastName = secondLastName;
    }

    public String getDcYear() {
        return dcYear;
    }

    public String getDcMonth() {
        return dcMonth;
    }

    public String getDcDay() {
        return dcDay;
    }

    public String getDfYear() {
        return dfYear;
    }

    public String getDfMonth() {
        return dfMonth;
    }

    public String getDfDay() {
        return dfDay;
    }
}
