package org.unicomer.stepDefinitions.Services;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.annotations.Managed;
import org.unicomer.constants.ConstantRest;
import org.unicomer.task.services.ExecutionGetSalesEvaluationUnicomer;
import org.unicomer.task.services.ExecutionGetUnicomer;

import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.equalTo;

public class ServicesRestGetSalesEvaluationUnicomer {
    @Managed
    private Actor admin = Actor.named("admin");

    @Before
    public void setup() {
        setTheStage(new OnlineCast());
    }

    @Given("the user test Unicomer Sales Evaluation")
    public void the_user_test_unicomer_sales_evaluation() {
        admin.whoCan(CallAnApi.at(ConstantRest.getUrlMambuParaguay()));
    }
    @When("use the Unicomer Sales Evaluation {string} with {string} and {string}")
    public void use_the_unicomer_sales_evaluation_with_and(String service, String id, String value) {
        admin.attemptsTo(ExecutionGetSalesEvaluationUnicomer.getInformation(service,id,value));

    }
    @Then("validate the status Unicomer Sales Evaluation {int}")
    public void validate_the_status_unicomer_sales_evaluation(Integer statuscode) {

    }

}

