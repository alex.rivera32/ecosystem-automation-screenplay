package org.unicomer.stepDefinitions.Services;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.annotations.Managed;
import org.unicomer.constants.ConstantRest;
import org.unicomer.task.services.ExecutionGetTokenUnicomer;

import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;

public class ServicesRestGetTokenUnicomer {
    @Managed
    private Actor admin = Actor.named("admin");

    @Before
    public void setup() {
        setTheStage(new OnlineCast());
    }

    @Given("the user test Unicomer Token {string}")
    public void the_user_test_unicomer_token(String url) {
        admin.whoCan(CallAnApi.at(url));
    }

    @When("use the Unicomer Token  {string}")
    public void use_the_unicomer_token(String service) {
        admin.attemptsTo(ExecutionGetTokenUnicomer.getNewToken(service));
    }

    @Then("validate the status Unicomer Token {int}")
    public void validate_the_status_unicomer_token(Integer status) {
        admin.should(seeThatResponse(response -> response.statusCode(status)));
    }

}

