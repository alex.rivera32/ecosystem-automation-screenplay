package org.unicomer.stepDefinitions.Services;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.annotations.Managed;
import org.unicomer.constants.ConstantRest;
import org.unicomer.task.services.ExecutionGetUnicomer;

import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.equalTo;

public class ServicesRestGetScoreRiskUnicomer {
    @Managed
    private Actor admin = Actor.named("admin");

    @Before
    public void setup() {
        setTheStage(new OnlineCast());
    }

    @Given("the user test Unicomer {string}")
    public void the_user_test_unicomer(String url) {
        admin.whoCan(CallAnApi.at(url));
    }
    @When("use the Unicomer  {string} with {string}")
    public void use_the_unicomer_with(String service, String id) {
        admin.attemptsTo(ExecutionGetUnicomer.getInformation(service,id));
    }
    @Then("validate the status Unicomer {int} and {string}")
    public void validate_the_status_unicomer_and(Integer status, String name) {
        admin.should(seeThatResponse(response -> response.statusCode(status).body("data.details.basicInfo.firstName",equalTo(name))));
    }

}

