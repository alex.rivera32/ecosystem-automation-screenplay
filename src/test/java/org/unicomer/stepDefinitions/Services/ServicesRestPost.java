package org.unicomer.stepDefinitions.Services;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.annotations.Managed;
import org.unicomer.task.services.ExecutionPostToken;
import io.cucumber.datatable.DataTable;

import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.equalTo;

public class ServicesRestPost {
    @Managed
    private Actor admin = Actor.named("admin");

    @Before
    public void setup() {
        setTheStage(new OnlineCast());
    }

    @Given("the user test Post {string}")
    public void the_user_test_post(String url) {
        admin.whoCan(CallAnApi.at(url));
    }

    @When("use the {string}")
    public void use_the(String endpoint, DataTable infoCustomer) {
        admin.attemptsTo(ExecutionPostToken.createUser(endpoint,infoCustomer));
    }

    @Then("validate the statusPost {int} and {string}")
    public void validate_the_status_post_and(Integer statusCode, String name) {
        admin.should(seeThatResponse(response -> response.statusCode(statusCode).body("name", equalTo(name))));
    }

}

