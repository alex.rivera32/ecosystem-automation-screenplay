package org.unicomer.stepDefinitions.Services;

import static net.serenitybdd.screenplay.actors.OnStage.setTheStage;
import static net.serenitybdd.screenplay.rest.questions.ResponseConsequence.seeThatResponse;
import static org.hamcrest.Matchers.equalTo;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.thucydides.core.annotations.Managed;
import org.unicomer.task.services.ExecutionGetToken;

public class ServicesRestGet {

    @Managed
    private Actor admin = Actor.named("admin");

    @Before
    public void setup() {
        setTheStage(new OnlineCast());
    }

    @Given("the user test {string}")
    public void the_user_test(String url) {
        admin.whoCan(CallAnApi.at(url));
    }

    @When("use the {string} with {string}")
    public void use_the_with(String service, String id) {
        admin.attemptsTo(ExecutionGetToken.getInformation(service,id));
    }
    @Then("validate the status {int} and {string}")
    public void validate_the_status(int status,String name) {
        admin.should(seeThatResponse(response -> response.statusCode(status).body("firstName",equalTo(name))));

    }

}

