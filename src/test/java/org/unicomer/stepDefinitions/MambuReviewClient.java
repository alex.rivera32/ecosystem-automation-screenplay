package org.unicomer.stepDefinitions;

import io.cucumber.java.Before;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import org.unicomer.task.mambu.Navigate;

import java.util.Map;

public class MambuReviewClient {

    @Managed
    private WebDriver hisBrowser;
    private Actor admin = Actor.named("admin");

    @Before
    public void setup() {
        admin.can(BrowseTheWeb.with(hisBrowser));
    }

    @Given("User navigate into Mambu {string} with the credentials")
    public void navigate_into_mambu(String mambuUrl) {
        admin.attemptsTo(Navigate.page(mambuUrl));
    }

}
