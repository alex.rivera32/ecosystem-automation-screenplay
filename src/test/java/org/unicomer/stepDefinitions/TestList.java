package org.unicomer.stepDefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import org.unicomer.utils.UtilsManager;

public class TestList {
    @Managed
    private WebDriver hisBrowser;
    private Actor admin = Actor.named("admin");

    @Before
    public void setup() {
        admin.can(BrowseTheWeb.with(hisBrowser));
    }

    @When("Test List")
    public void test_list() {
        //UtilsManager.getInstance().readExcel(1);
    }


}

