package org.unicomer.stepDefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;
import org.openqa.selenium.WebDriver;
import org.unicomer.models.Customer;
import org.unicomer.questions.TheMessage;
import org.unicomer.task.emma.*;
import org.unicomer.utils.UtilsManager;

import java.util.HashMap;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

public class NewClientApplication {

    private Customer customer;
    @Managed
    private WebDriver hisBrowser;
    private Actor admin = Actor.named("admin");

    @Before
    public void setup() {
        admin.can(BrowseTheWeb.with(hisBrowser));
    }

    @Given("Excel customer data {int}")
    public void customer_data(int row) {
        System.out.println(row);
        HashMap customerData = (HashMap) UtilsManager.getInstance().readExcel(row);
        customer = new Customer(customerData);
    }

    @Given("DataTable customer data")
    public void customer_data(Customer customer) {
        this.customer = customer;
    }

    @Given("the user into EMMA {string} with the credentials")
    public void the_user_into_emma_with_the_credentials(String emma) {
        admin.attemptsTo(Navigate.page(emma));
    }

    @Given("select the country")
    public void select_the_country() {
        admin.attemptsTo(SelectCountry.the());
    }

    @When("fill out customer data")
    public void fill_out_customer_data() {
        admin.attemptsTo(FilloutDocumentInformation.with(customer));
        admin.attemptsTo(FilloutBasicInformation.customer(customer));
    }

    @When("fill out additional information of the customer")
    public void fill_out_additional_information_of_the_customer(DataTable dataCustomer) {
        admin.attemptsTo(FilloutAdditionalInformation.customer(dataCustomer));
    }

    @When("fill out work information")
    public void fill_out_work_information(DataTable dataCustomer) {
        admin.attemptsTo(FilloutWorkInformation.customer(dataCustomer));
    }

    @When("fill out additional home information")
    public void fill_out_additional_home_information(DataTable dataCustomer) {
        admin.attemptsTo(FilloutAdditionalInformationHome.customer(dataCustomer));
    }

    @When("fill out information about references")
    public void fill_out_information_about_references(DataTable dataCustomer) {
        admin.attemptsTo(FilloutReferenceInformation.customer(dataCustomer));
    }

    @When("upload the documents for the {string}")
    public void upload_the_documents_for_the(String lineCredit) {
        admin.attemptsTo(FilloutUploadDocuments.customer(lineCredit));
    }

    @Then("validate the creation of the application {string}")
    public void validate_the_creation_of_the_application(String message) {
        admin.should(seeThat(TheMessage.is(),equalTo(message)));
    }

}

