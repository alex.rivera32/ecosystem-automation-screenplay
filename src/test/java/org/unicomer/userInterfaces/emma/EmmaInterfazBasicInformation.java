package org.unicomer.userInterfaces.emma;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public final class EmmaInterfazBasicInformation {
    public static final Target LIST_SELECT = Target.the("ALERT COUNTRY").located(By.id("mat-select-0"));
    public static final Target LIST_ELEMENTS = Target.the("LIST").located(By.xpath("//span[@class='mat-option-text']"));
    public static final Target INPUT_DOCUMENT_CLIENT = Target.the("INSERT DOCUMENT").located(By.id("inputDoc"));
    public static final Target BTN_CONTINUE = Target.the("BUTTON CONTINUE").located(By.xpath("//span[contains(text(),'CONTINUE')]"));
    public static final Target BTN_SEARCH_CUSTOMER = Target.the("BUTTON SEARCH CUSTOMER").located(By.xpath("//button[contains(@class, 'mat-focus-indicator calcButton w-100 mat-raised-button mat-button-base mat-primary')]"));
    public static final Target BTN_START_APPLICATION = Target.the("BUTTON START APPLICATION").located(By.xpath("//button[contains(@class, 'mat-focus-indicator btn-primary w-100 mat-button mat-button-base')]"));
    public static final Target LIST_LINE_CREDIT = Target.the("LIST LINE CREDIT").located(By.id("30eda6c6-f390-46be-a11a-2a2a7f9bfa58"));
    public static final Target INPUT_NAME = Target.the("INPUT NAME").located(By.xpath("//input[@name='17f4b861-1f18-4230-8e80-bd6b27bcc8d7']"));
    public static final Target INPUT_LAST_NAME = Target.the("INPUT THE LAST NAME").located(By.xpath("//input[@name='e603f0a9-3aa1-4994-af4d-36e5b5d676b5']"));
    public static final Target LIST_GENDER = Target.the("LIST GENDER").located(By.id("eb061ebb-e8c7-4aec-b23d-9bb4ae0bd2d1"));
    public static final Target LIST_CIVIL_STATUS = Target.the("LIST CIVIL STATUS").located(By.id("d2f8a854-925f-4454-b3f4-58a9c1c4cf40"));
    public static final Target LIST_BIRTH_DATE = Target.the("LIST BIRTH DATE").located(By.xpath("//input[@name='efabefc3-6414-41cf-8e53-a398ed6d2a3a']"));
    public static final Target BTN_SELECT_YEARS = Target.the("BUTTON SELECT YEARS ").located(By.xpath("//button[contains(@class,'mat-focus-indicator mat-calendar-period-button mat-button mat-button-base')]"));
    public static final Target LIST_SELECT_DATE = Target.the("BUTTON SELECT YEAR").located(By.xpath("//div[contains(@class,'mat-calendar-body-cell-content mat-focus-indicator')]"));
    public static final Target INPUT_PHONE_NUMBER = Target.the("INPUT PHONE NUMBER").located(By.xpath("//input[@name='a87e80b8-e769-48f0-b84d-efa2371060e9']"));
    public static final Target INPUT_EMAIL = Target.the("INPUT EMAIL").located(By.xpath("//input[@name='a6e3504e-3ef9-4c8e-b1f0-6f227624213a']"));
    public static final Target LIST_SOURCE_INCOME = Target.the("LIST SOURCE INCOME").located(By.id("1a621d7c-bb00-4250-9c25-a647d4af8efa"));
    public static final Target LIST_FREQUENCY_PAID = Target.the("LIST FREQUENCY PAID").located(By.id("903b9260-9bdd-453a-b412-11732acd588d"));
    public static final Target INPUT_INCOME = Target.the("INPUT INCOME").located(By.xpath("//input[@name='05e70459-4468-41c3-befe-86a172cb737f']"));
    public static final Target LIST_DEPARTMENT = Target.the("LIST DEPARTMENT").located(By.id("9b96dddd-b836-4b5b-b84c-02f11e8c6558"));
    public static final Target LIST_DISTRICT = Target.the("LIST DISTRICT").located(By.id("84cce585-3076-4a4c-b418-c3752cdc49a7"));
    public static final Target LIST_NEIGHBORHOOD = Target.the("LIST NEIGHBORHOOD").located(By.id("2b992353-2ada-4017-889b-cda3ced01fa0"));
    public static final Target BTN_UPLOAD_DOCUMENT = Target.the("BUTTON UPLOAD DOCUMENT").located(By.xpath("//div[contains(@class,'uploadDoc')]"));
    public static final Target INPUT_DOCUMENT = Target.the("INPUT DOCUMENT FILE").located(By.xpath("//input[contains(@type,'file')]"));
    public static final Target BTN_SEND_FOR_EVALUATION = Target.the("BUTTON SEND FOR EVALUATION").located(By.xpath("//span[contains(text(),'GUARDAR INFO')]"));
    public static final Target BTN_PREAPPROVED_OK = Target.the("BUTTON PREAPROVED OK").located(By.id("preApprovedOk"));
    public static final Target LIST_DOCUMENT_CREATION_DATE = Target.the("LIST DOCUMENT CREATION DATE").located(By.xpath("//input[@name='1f390536-a9af-46b1-9593-99569a840e02']"));
    public static final Target LIST_DOCUMENT_FINAL_DATE = Target.the("LIST DOCUMENT FINAL DATE").located(By.xpath("//input[@name='8120bf4d-5ce8-454e-9481-bd3b87a711e9']"));

}
