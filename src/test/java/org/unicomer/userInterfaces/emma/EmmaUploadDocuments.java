package org.unicomer.userInterfaces.emma;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EmmaUploadDocuments {

    public static final Target LIST_LINE_CREDIT_DOCUMENTS = Target.the("LIST LINE CREDIT DOCUMENTS").located(By.id("af8739a0-3e02-4de4-9832-efc7430e50c1"));
    public static final Target LIST_ELEMENTS_DOCUMENTS = Target.the("LIST").located(By.xpath("//span[ @class='mat-option-text']"));
    public static final Target INPUT_DOCUMENT = Target.the("INPUT DOCUMENT FILE").located(By.xpath("//input[contains(@type,'file')]"));
    public static final Target BTN_DOCUMENT_1 = Target.the("BUTTON DOCUMENT 1").located(By.xpath("//span[contains(text(), 'Frente del documento de identidad del cliente')]"));
    public static final Target BTN_DOCUMENT_2 = Target.the("BUTTON DOCUMENT 2").located(By.xpath("//span[contains(text(), 'Dorso del documento de identidad del cliente')]"));
    public static final Target BTN_DOCUMENT_3 = Target.the("BUTTON DOCUMENT 3").located(By.xpath("//span[contains(text(), 'Ingrese la boleta de ANDE')]"));
    public static final Target BTN_DOCUMENT_4 = Target.the("BUTTON DOCUMENT 4").located(By.xpath("//span[contains(text(), 'Ingrese un comprobante de sus ingresos')]"));
    public static final Target BTN_DOCUMENT_5 = Target.the("BUTTON DOCUMENT 5").located(By.xpath("//span[contains(text(), '1')]"));
    public static final Target BTN_DOCUMENT_6 = Target.the("BUTTON DOCUMENT 6").located(By.xpath("//span[contains(text(), '2')]"));
    public static final Target BTN_DOCUMENT_7 = Target.the("BUTTON DOCUMENT 7").located(By.xpath("//span[contains(text(), '3')]"));
    public static final Target BTN_DOCUMENT_8 = Target.the("BUTTON DOCUMENT 8").located(By.xpath("//span[contains(text(), '4')]"));
    public static final Target BTN_SENT_APLICATION_FINISH = Target.the("BUTTON FINISH APLICATION").located(By.id("button-form-credit-qualification"));
    public static final Target ALERT_CLIENT_CREATE = Target.the("ALERT CLIENT CREATE").located(By.xpath("//p[@class='p1']"));

}
