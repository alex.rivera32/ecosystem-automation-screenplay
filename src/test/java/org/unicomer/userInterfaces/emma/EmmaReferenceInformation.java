package org.unicomer.userInterfaces.emma;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class EmmaReferenceInformation {

    public static final Target INPUT_NAME_FIRST_REFERENCE = Target.the("INPUT FIRST REFERENCE").located(By.xpath("//input[@name='8c6d2ace-2157-4c8e-8e7b-4726eb7c8bf2']"));
    public static final Target LIST_RELATIONSHIP_FIRST_REFERENCE = Target.the("LIST REFERENCE RELATIONSHIP").located(By.id("4bee5908-10ea-4ff6-aa6a-b2c44af3b09c"));
    public static final Target LIST_ELEMENTS = Target.the("LIST").located(By.xpath("//span[ @class='mat-option-text']"));
    public static final Target LIST_TYPE_PHONE_FIST_REFERENCE = Target.the("LIST TYPE PHONE").located(By.id("19b2ac60-8f88-4225-a4eb-64bc3bfdcfac"));
    public static final Target INPUT_PHONE_FIRST_REFERENCE = Target.the("INPUT PHONE FIRST REFERENCE").located(By.xpath("//input[@name='081eb726-b254-4476-aad7-2bf94f8d1dff']"));
    public static final Target INPUT_NAME_SECOND_REFERENCE = Target.the("INPUT SECOND REFERENCE").located(By.xpath("//input[@name='bd098e62-160c-4b6e-a6ee-f30122bae2d0']"));
    public static final Target LIST_RELATIONSHIP_SECOND_REFERENCE = Target.the("LIST SECOND REFERENCE RELATIONSHIP").located(By.id("2edcc759-7ad2-463f-9cfa-565f772f9592"));
    public static final Target LIST_TYPE_PHONE_SECOND_REFERENCE = Target.the("LIST SECOND TYPE PHONE").located(By.id("40007bd1-16ba-4912-8542-63c10cbf2fbf"));
    public static final Target INPUT_PHONE_SECOND_REFERENCE = Target.the("INPUT PHONE SECOND REFERENCE").located(By.xpath("//input[@name='52a4840d-ac5f-4fc0-9c60-773a9f465cdf']"));
    public static final Target INPUT_NAME_THIRD_REFERENCE = Target.the("INPUT FIRST REFERENCE").located(By.xpath("//input[@name='148468ec-4def-46e0-98e4-c6fbc75231fb']"));
    public static final Target LIST_RELATIONSHIP_THIRD_REFERENCE = Target.the("LIST REFERENCE RELATIONSHIP").located(By.id("2d7c733c-511f-471b-95c1-bb9cb7a2cf31"));
    public static final Target LIST_TYPE_PHONE_THIRD_REFERENCE = Target.the("LIST TYPE PHONE").located(By.id("8aecd4d9-3fb2-4b75-b932-7720aed191f4"));
    public static final Target INPUT_PHONE_THIRD_REFERENCE = Target.the("INPUT PHONE FIRST REFERENCE").located(By.xpath("//input[@name='957b016c-9496-496c-84ec-5df5823940ec']"));
    public static final Target BTN_NEXT_DOCUMENTS = Target.the("BUTTON CONTINUE REFERENCE").located(By.id("button-form-credit-qualification"));

}
