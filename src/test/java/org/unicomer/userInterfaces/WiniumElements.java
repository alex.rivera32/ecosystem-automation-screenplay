package org.unicomer.userInterfaces;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class WiniumElements {

    public static final Target INPUT_USERNAME = Target.the("ENTER THE USERNAME").located(By.name("username"));
}
