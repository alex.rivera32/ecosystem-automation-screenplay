package org.unicomer.interactions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.unicomer.constants.SystemConstants;
import org.unicomer.models.Customer;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ReadExcelInteractions implements Interaction {

    @Override
    public <T extends Actor> void performAs(T actor) {
        try {
            String excelPath = SystemConstants.EXCEL_PATH;
            FileInputStream file = new FileInputStream(new File(excelPath));
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            XSSFSheet sheet = workbook.getSheetAt(0);

            Iterator<Cell> keyIterator =  sheet.getRow(0).cellIterator();
            int x = 0;
            Map customer = new HashMap<>();

            while (keyIterator.hasNext())
            {
                String key = keyIterator.next().getStringCellValue();
                String value =  sheet.getRow(0).getCell(x).getStringCellValue();

                customer.put(key,value);
                x = x + 1;
                System.out.println(key + " - " + value);

            }
            System.out.println("");
            file.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void setValue(String property,String value){
        Customer customer = new Customer(property,value);
    }

    public static ReadExcelInteractions init(){
        return new ReadExcelInteractions();
    }


}
