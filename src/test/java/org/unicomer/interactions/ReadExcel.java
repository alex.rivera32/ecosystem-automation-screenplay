package org.unicomer.interactions;


import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.*;
import org.unicomer.models.Customer;

import java.io.*;
import java.util.Iterator;

public class ReadExcel {

    public static void main (String [ ] args) throws IOException {
        String pathDocumentExcel = System.getProperty("user.dir");
        pathDocumentExcel = pathDocumentExcel+"\\src\\test\\resources\\files\\data\\CustomerEmma.xlsx";
        System.out.println(pathDocumentExcel);

            FileInputStream inputStream = new FileInputStream(new File(pathDocumentExcel));
            Workbook workbook = new XSSFWorkbook(inputStream);
            Sheet firstSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = firstSheet.iterator();
            DataFormatter formatter = new DataFormatter();

        for (int i = 0; i < firstSheet.getLastRowNum(); i++) {
            for (int j = 0; j < firstSheet.getRow(i).getLastCellNum(); j++) {
                Row newRow = firstSheet.getRow(i);
                Cell newCell = newRow.getCell(j);
                Row newRowValue = firstSheet.getRow(1);
                Cell newCellValue = newRowValue.getCell(j);
                String contentCell = formatter.formatCellValue(newCell);
                String contentCell2 = formatter.formatCellValue(newCellValue);
                ReadExcel.setValue(contentCell,contentCell2);
            }

        }
    }

    public static void setValue(String property,String value){
        Customer customer = new Customer(property,value);
    }

}
