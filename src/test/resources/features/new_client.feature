@Paraguay
Feature: New client

  @ReviewClientMambu
  Scenario Outline: Verify client exist in mambu application
    #Given User navigate into Mambu "<country>" with the credentials
    Given Admin user validate credentials username and password
      | username    | password    |
      | <username>  | <password>  |
    Examples:
      | country  | username  | password  |
      | Paraguay | Rigoberto | Rigoberto |

  @NewClientEmma2
  Scenario Outline: New credit application Paraguay
    Given DataTable customer data
      | typeId   | documentId   | creditLine   | firstName   | secondName   | firstLastName   | secondLastName   | gender   | civilStatus   | birthDate   | numberMovil   | email   | sourceIncome   | frecuencyPaid   | income   | department   | district   | neighborhood   | country   | dependents   | typePhoneAddionalInf   | numberPhoneAdditional   | docCreationDate   | docFinalDate   |
      | <typeId> | <documentId> | <creditLine> | <firstName> | <secondName> | <firstLastName> | <secondLastName> | <gender> | <civilStatus> | <birthDate> | <numberMovil> | <email> | <sourceIncome> | <frecuencyPaid> | <income> | <department> | <district> | <neighborhood> | <country> | <dependents> | <typePhoneAddionalInf> | <numberPhoneAdditional> | <docCreationDate> | <docFinalDate> |
    Given the user into EMMA "<country>" with the credentials
    And select the country
    When fill out customer data
    And fill out additional information of the customer
      | country   | dependents   | typePhoneAddionalInf   | numberPhoneAdditional   |
      | <country> | <dependents> | <typePhoneAddionalInf> | <numberPhoneAdditional> |
    And fill out work information
      | nameCompany   | dateCompanyYear   | dateCompanyMonth   | dateCompanyDay   | occupation   | department   | district   | neighborhood   | streetWork   | referenceWork   | typePhoneWork1   | numberphonework1   | typePhoneWork2   | numberphonework2   |
      | <nameCompany> | <dateCompanyYear> | <dateCompanyMonth> | <dateCompanyDay> | <occupation> | <department> | <district> | <neighborhood> | <streetWork> | <referenceWork> | <typePhoneWork1> | <numberphonework1> | <typePhoneWork2> | <numberphonework2> |
    And fill out additional home information
      | streetHome   | referenceHome   | dateHomeYear      | dateHomeMonth      | typeHome   |
      | <streetWork> | <referenceWork> | <dateCompanyYear> | <dateCompanyMonth> | <typeHome> |
    And fill out information about references
      | nameReference1   | relationship1   | typePhonereference1   | numberReference1   | nameReference2   | relationship2   | typePhonereference2   | numberReference2   | nameReference3   | relationship3   | typePhonereference3   | numberReference3   |
      | <nameReference1> | <relationship1> | <typePhonereference1> | <numberReference1> | <nameReference2> | <relationship2> | <typePhonereference2> | <numberReference2> | <nameReference3> | <relationship3> | <typePhonereference3> | <numberReference3> |
    And upload the documents for the "<creditLine>"
    Then validate the creation of the application "<message>"
    #Es necesario cambiar los datos del ejemplo para que pueda crear al cliente.
    Examples:
      | country  | typeId              | documentId | creditLine                  | firstName | secondName | firstLastName | secondLastName | gender    | civilStatus | birthDate  | numberMovil | email             | sourceIncome        | frecuencyPaid | income   | department | district | neighborhood | dependents | typePhoneAddionalInf | numberPhoneAdditional | nameCompany | dateCompanyYear | dateCompanyMonth | dateCompanyDay | occupation      | streetWork | referenceWork | typePhoneWork1         | numberphonework1 | typePhoneWork2         | numberphonework2 | typeHome | nameReference1 | relationship1 | typePhonereference1       | numberReference1 | nameReference2 | relationship2 | typePhonereference2       | numberReference2 | nameReference3 | relationship3 | typePhonereference3       | numberReference3 | message                                                 | docCreationDate | docFinalDate |
      | Paraguay | Cédula de Identidad | 1199212    | Línea de crédito de consumo | David     | Jhon       | Campos        | Vega           | Masculino | Soltero     | 2000-MAY-7 | 0982326326  | prueba8@gmail.com | Dependiente con IPS | Mensual       | 15000000 | ASUNCION   | ASUNCION | SAJONIA      | 0          | Teléfono móvil       | 0981326326            | Unicomer    | 2000            | JUN.             | 11             | Administrador/a | Calle 5    | Building      | Teléfono móvil trabajo | 0981546326       | Teléfono móvil trabajo | 0981536328       | Propia   | Madre Cliente  | Madre         | Teléfono móvil referencia | 0984563623       | Padre Cliente  | Padre         | Teléfono móvil referencia | 0981236536       | Amigo Cliente  | Amigo/a       | Teléfono móvil referencia | 0981563265       | ¡La aplicación al crédito ha sido completada con éxito! | 2020-MAY-7      | 2026-MAY-7   |

  @NewClientEmma3
  Scenario Outline: New credit application Paraguay
    Given DataTable customer data
      | typeId   | documentId   | creditLine   | firstName   | secondName   | firstLastName   | secondLastName   | gender   | civilStatus   | birthDate   | numberMovil   | email   | sourceIncome   | frecuencyPaid   | income   | department   | district   | neighborhood   | country   | dependents   | typePhoneAddionalInf   | numberPhoneAdditional   |
      | <typeId> | <documentId> | <creditLine> | <firstName> | <secondName> | <firstLastName> | <secondLastName> | <gender> | <civilStatus> | <birthDate> | <numberMovil> | <email> | <sourceIncome> | <frecuencyPaid> | <income> | <department> | <district> | <neighborhood> | <country> | <dependents> | <typePhoneAddionalInf> | <numberPhoneAdditional> |
    Given the user into EMMA "<country>" with the credentials
    And select the country
    When fill out customer data
    And fill out additional information of the customer
      | country   | dependents   | typePhoneAddionalInf   | numberPhoneAdditional   |
      | <country> | <dependents> | <typePhoneAddionalInf> | <numberPhoneAdditional> |
    And fill out work information
      | nameCompany   | dateCompanyYear   | dateCompanyMonth   | dateCompanyDay   | occupation   | department   | district   | neighborhood   | streetWork   | referenceWork   | typePhoneWork1   | numberphonework1   | typePhoneWork2   | numberphonework2   |
      | <nameCompany> | <dateCompanyYear> | <dateCompanyMonth> | <dateCompanyDay> | <occupation> | <department> | <district> | <neighborhood> | <streetWork> | <referenceWork> | <typePhoneWork1> | <numberphonework1> | <typePhoneWork2> | <numberphonework2> |
    And fill out additional home information
      | streetHome   | referenceHome   | dateHomeYear      | dateHomeMonth      | typeHome   |
      | <streetWork> | <referenceWork> | <dateCompanyYear> | <dateCompanyMonth> | <typeHome> |
    And fill out information about references
      | nameReference1   | relationship1   | typePhonereference1   | numberReference1   | nameReference2   | relationship2   | typePhonereference2   | numberReference2   | nameReference3   | relationship3   | typePhonereference3   | numberReference3   |
      | <nameReference1> | <relationship1> | <typePhonereference1> | <numberReference1> | <nameReference2> | <relationship2> | <typePhonereference2> | <numberReference2> | <nameReference3> | <relationship3> | <typePhonereference3> | <numberReference3> |
    And upload the documents for the "<creditLine>"
    Then validate the creation of the application "<message>"
    #Es necesario cambiar los datos del ejemplo para que pueda crear al cliente.
    Examples:
      | country  | typeId              | documentId | creditLine                    | firstName | secondName | firstLastName | secondLastName | gender    | civilStatus | birthDate  | numberMovil | email             | sourceIncome        | frecuencyPaid | income   | department | district | neighborhood | dependents | typePhoneAddionalInf | numberPhoneAdditional | nameCompany | dateCompanyYear | dateCompanyMonth | dateCompanyDay | occupation      | streetWork | referenceWork | typePhoneWork1         | numberphonework1 | typePhoneWork2         | numberphonework2 | typeHome | nameReference1 | relationship1 | typePhonereference1       | numberReference1 | nameReference2 | relationship2 | typePhonereference2       | numberReference2 | nameReference3 | relationship3 | typePhonereference3       | numberReference3 | message                                                 |
      | Paraguay | Cédula de Identidad | 1199181    | Línea de crédito microcrédito | David     | Jhon       | Campos        | Vega           | Masculino | Soltero     | 2000-MAY-7 | 0982326326  | prueba8@gmail.com | Dependiente con IPS | Mensual       | 2000000 | ASUNCION   | ASUNCION | SAJONIA      | 0          | Teléfono móvil       | 0981326326            | Unicomer    | 2000            | JUN             | 11             | Administrador/a | Calle 5    | Building      | Teléfono móvil trabajo | 0981546326       | Teléfono móvil trabajo | 0981536328       | Propia   | Madre Cliente  | Madre         | Teléfono móvil referencia | 0984563623       | Padre Cliente  | Padre         | Teléfono móvil referencia | 0981236536       | Amigo Cliente  | Amigo/a       | Teléfono móvil referencia | 0981563265       | ¡La aplicación al crédito ha sido completada con éxito! |

  @ExampleListwithExcel
  Scenario: Test for List
    When Test List
