@Paraguay
Feature: New client

  @NewClientEmma2
  Scenario Outline: New credit application Paraguay
    Given Excel customer data <row>
    Given the user into EMMA "Paraguay" with the credentials
    And select the country
    When fill out customer data
    And fill out additional information of the customer

    #Es necesario cambiar los datos del ejemplo para que pueda crear al cliente.
    Examples:
      | row |
      | 1   |