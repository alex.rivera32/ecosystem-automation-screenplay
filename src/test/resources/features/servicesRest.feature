@Paraguay
Feature: New client for Service Rest

  @TestRestGet
  Scenario Outline: Test Rest Get
    Given the user test "<url>"
    When use the "<service>" with "<id>"
    Then validate the status <status> and "<name>"

    Examples:
      | url                   | service          | id      | status | name         |
      | https://gorest.co.in/ | public/v2/users/ | 3620139 | 200    | Carlos Perez |

  @TestRestPost
  Scenario Outline: Test Rest Post
    Given the user test Post "<url>"
    When use the "<service>"
      | name   | <name>   |
      | gender | <gender> |
      | email  | <email>  |
      | status | <status> |
    Then validate the statusPost <statusService> and "<validation>"

    Examples:
      | url                   | service          | name        | gender | email                        | status | statusService | validation  |
      | https://gorest.co.in/ | public/v2/users/ | Pedro Perez | Male   | prueba2023120723257@test.com | Active | 201           | Pedro Perez |

  @TestRestPostTokenUnicomer
  Scenario Outline: Test Rest Get Token
    Given the user test Unicomer Token "<url>"
    When use the Unicomer Token  "<service>"
    Then validate the status Unicomer Token <status>
    Examples:
      | url                             | service       | status |
      | https://apigee-e2e.unicomer.com | /oauth2/token | 200    |

  @TestRestGetUnicomer
  Scenario Outline: Test Rest Get
    Given the user test Unicomer "<url>"
    When use the Unicomer  "<service>" with "<id>"
    Then validate the status Unicomer <status> and "<name>"

    Examples:
      | url                             | service                                                                                  | id      | status | name   |
      | https://apigee-e2e.unicomer.com | /credit/creditbureau/v1/customers/score-risk?documentType=&personType=PN&documentNumber= | 1090501 | 200    | HERMES |

  @TestRestGetSalesUnicomer
  Scenario Outline: Test Rest Get
    Given the user test Unicomer Sales Evaluation
    When use the Unicomer Sales Evaluation "<service>" with "<id>" and "<value>"
    Then validate the status Unicomer Sales Evaluation <status>

    Examples:
      | service                                                                                  | id      | status | value|
      | /credit/creditbureau/v1/customers/score-risk?documentType=&personType=PN&documentNumber= | 1090501 | 200    | 50000|

