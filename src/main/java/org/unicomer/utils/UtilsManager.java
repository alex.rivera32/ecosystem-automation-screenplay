package org.unicomer.utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.unicomer.constants.SystemConstants;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class UtilsManager {

    private static UtilsManager instance;
    private String id_trn;

    private UtilsManager(){
        setTRN();
    }
    private String getRandomId(int min, int max){

        Integer randomInt = (int)Math.floor(Math.random() * (max - min + 1) + min);
        return randomInt.toString();
    }

    public static synchronized UtilsManager getInstance(){
        if(instance == null){
            instance = new UtilsManager();
        }
        return instance;
    }

    public Map readExcel(int row){
        try {
            String excelPath = SystemConstants.EXCEL_PATH;
            FileInputStream file = new FileInputStream(new File(excelPath));
            XSSFWorkbook workbook = new XSSFWorkbook(file);

            XSSFSheet sheet = workbook.getSheetAt(0);
            Iterator<Cell> keyIterator =  sheet.getRow(0).cellIterator();
            int x = 0;
            Map dataFile = new HashMap<>();

            while (keyIterator.hasNext())
            {
                String key = keyIterator.next().getStringCellValue();
                String value =  sheet.getRow(row).getCell(x).getStringCellValue();

                dataFile.put(key,value);
                System.out.println(key + " - " + value);
                x = x + 1;

            }
            file.close();
            return dataFile;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getEmail(){
        String format="yyyy-MM-dd HH:mm:ss";
        DateTimeFormatter date = DateTimeFormatter.ofPattern(format);
        LocalDateTime now = LocalDateTime.now();
        String fecha = date.format(now);
        fecha=fecha.replace("-","");
        fecha=fecha.replace(" ","");
        fecha=fecha.replace(":","");
        return "correo"+fecha+"@gmail.com";
    }

    private void setTRN(){
        id_trn = getRandomId(100000000, 999999999);
    }

    public void setTRN(String document){
        id_trn = document;
    }


    public String getTRN(){
        return id_trn;
    }

    public void setPassport(){
        this.id_trn = this.id_trn + "9";
    }

    public String getPathDocument(){
        String pathDocument = System.getProperty("user.dir");
        System.out.println(pathDocument+SystemConstants.DOCUMENT_PATH);
        return pathDocument+"\\"+SystemConstants.DOCUMENT_PATH;
    }

}
