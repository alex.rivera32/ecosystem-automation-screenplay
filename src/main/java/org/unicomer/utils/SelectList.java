package org.unicomer.utils;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.questions.Visibility;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import net.serenitybdd.screenplay.targets.Target;

import java.util.List;

public class SelectList implements Interaction {
    private static final String ELEMENTS = "//span[@class='mat-option-text']";
    private static final String ELEMENTS_DATE = "//div[contains(@class,'mat-calendar-body-cell-content mat-focus-indicator')]";
    private static final String ELEMENTS_COUNTRY = "//span[@class='ng-option-label ng-star-inserted']";
    private static final String ELEMENTS_OCUPATION = "//div[contains(@class,'ng-option ng-star-inserted')]";
    private final Target list;
    private final String opcion;
    private final String typeElement;
    public SelectList(Target list, String opcion, String typeElement) {
        this.list=list;
        this.opcion=opcion;
        this.typeElement=typeElement;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        boolean isVisible = actor.asksFor(Visibility.of(By.xpath(typeElement)));
        if(!isVisible){
            System.out.println("EL ERROR NO ES EL PATH DE LA LISTA, SI NO EL CLICK ANTERIOR");
            return;
        }
        List<WebElement> listObject = list.resolveFor(actor).findElements(By.xpath(typeElement));
        System.out.println(opcion);
        for ( int i=0; i<listObject.size(); i++){
            if (listObject.get(i).getText().equals(opcion)){
                listObject.get(i).click();
                break;
            }
        }
    }

    public static String getElement(){
        return ELEMENTS;
    }

    public static String getDate(){
        return ELEMENTS_DATE;
    }

    public static String getCountry(){
        return ELEMENTS_COUNTRY;
    }

    public static String getOcupation(){
        return ELEMENTS_OCUPATION;
    }

    public static SelectList since(Target list,String opcion,String typeElement){
        return new SelectList(list,opcion,typeElement);
    }


}
