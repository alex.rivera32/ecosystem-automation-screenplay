package org.unicomer.constants;

public class ConstantRest {

    public static String URL_MAMBU_PARAGUAY = "https://apigee-e2e.unicomer.com/";
    public static String TOKEN = null;

    public static String getUrlMambuParaguay() {
        return URL_MAMBU_PARAGUAY;
    }

    public static String getTOKEN() {
        return TOKEN;
    }

    public static void setTOKEN(String TOKEN) {
        ConstantRest.TOKEN = TOKEN;
    }



}
