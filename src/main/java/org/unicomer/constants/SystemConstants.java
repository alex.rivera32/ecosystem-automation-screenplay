package org.unicomer.constants;

public final class SystemConstants {
    private SystemConstants(){}
    public static final String MAMBU_URL = "MAMBU.URL";
    public static final String MAMBU_USERNAME = "MAMBU.USERNAME";
    public static final String MAMBU_PASSWORD = "MAMBU.PASSWORD";
    public static final String COUNTRY = "COUNTRY";
    public static final String EMMA_USERNAME = "EMMA.USERNAME";
    public static final String EMMA_PASSWORD = "EMMA.PASSWORD";
    public static final String EMMA_URL = "EMMA.URL";
    public static final String WORKF_URL = "WORKF.URL";
    public static final String WORKF_USERNAME = "WORKF.USERNAME";
    public static final String WORKF_PASSWORD = "WORKF.PASSWORD";
    public static final String EXCEL_PATH = "src/test/resources/files/data/CustomerEmma.xlsx";
    public static final String DOCUMENT_PATH = "src\\test\\resources\\files\\dni.PNG";

}
